import java.util.Scanner;
public class JavaProject4 {
	
		   public static void main(String args[]) {
		   
		      int n, c;
		      System.out.print("Enter the user input : ");
		     
		      Scanner multiplication = new Scanner(System.in);
		      n = multiplication.nextInt();
		      System.out.println();
		      System.out.println("Multiplication table of "+n+" is :");
		 
		      for ( c = 1 ; c <= 16 ; c++ )
		         System.out.println(n + "*" +c+ " = " +(n*c));
		   }
		}
		 

